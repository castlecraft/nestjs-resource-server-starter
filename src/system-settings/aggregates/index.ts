import { SettingsService } from './settings/settings.service';
import { HealthCheckAggregateService } from './health-check/health-check.service';
import { DatabaseHealthIndicatorService } from './database-health-indicator/database-health-indicator.service';

export const SystemSettingsAggregates = [
  SettingsService,
  HealthCheckAggregateService,
  DatabaseHealthIndicatorService,
];
