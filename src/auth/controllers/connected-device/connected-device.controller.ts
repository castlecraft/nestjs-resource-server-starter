import {
  Controller,
  Query,
  Get,
  Res,
  Post,
  UseGuards,
  HttpCode,
  HttpStatus,
  Body,
} from '@nestjs/common';
import { ApiBearerAuth, ApiResponse } from '@nestjs/swagger';
import { RevokeTokenDto } from '../../../common/dtos/revoke-token.dto';
import { ConnectedDeviceService } from '../../aggregates/connected-device/connected-device.service';
import { TokenGuard } from '../../guards/token.guard';

@Controller('connected_device')
export class ConnectedDeviceController {
  constructor(private readonly service: ConnectedDeviceService) {}

  @Get('callback')
  @ApiResponse({
    status: HttpStatus.FOUND,
    description: 'Redirects state and code to custom uri',
  })
  async relayCodeAndState(
    @Query('code') code: string,
    @Query('state') state: string,
    @Res() res,
  ) {
    return await this.service.relayCodeAndState(code, state, res);
  }

  @Post('revoke_token')
  @ApiBearerAuth()
  @ApiResponse({ status: HttpStatus.OK, description: 'Revoke Access Token' })
  @HttpCode(HttpStatus.OK)
  @UseGuards(TokenGuard)
  async revokeToken(@Body() payload: RevokeTokenDto) {
    await this.service.revokeToken(payload.token);
  }
}
